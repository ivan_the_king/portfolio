const path = require("path");

const DIST_DIR   = path.join(__dirname, "server", "dist"),
const CLIENT_DIR = path.join(__dirname, "client");

module.exports = {
    context: CLIENT_DIR,

    entry: "./application",

    output: {
        path:     DIST_DIR,
        filename: "main.js"
    },

    resolve: {
        extensions: ['', '.js']
    }
};
